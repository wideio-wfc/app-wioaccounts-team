#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *



@wideio_owned()
@wideiomodel
@wideio_setnames("team membership request") # FIXME: DEPRECATE THIS DECORATOR
class TeamMembershipRequest(models.Model):
    team = models.ForeignKey('accounts.Team')

    def on_add(self, request):
        if team.teaminvitation_set.filter(to_user=self.owner):
           team = self.team
           team.members.add(self.owner)
           self.delete()
        self.owner=request.user

        from wioframework.utils import wio_send_email
        wio_send_email(request, "email_team_request",
                               self.owner+" would like to join the team "+str(self.team),
                                "mails/team_request.html",
                                {'requester':self.owner,'team':self.team,'x': self},
                              list(self.team.admins.all()))


    class WIDEIO_Meta:
        icon = "icon-group"
        permissions = dec.perm_for_logged_users_only
        MOCKS = {
            'default': [ {
                 'team': 'team-0000',
                 'owner': 'user-0002'
               }
            ]
        }

        class Actions:
            @wideio_action(icon="icon-ok",
                           possible=lambda s,
                           r: (r.user.is_authenticated() and r.user in s.team.members.all()))
            def approve(self, request):
                team = self.team
                team.members.add(self.owner)
                self.delete()
                from wioframework.utils import wio_send_email
                wio_send_email(request, "email_team_approved",
                               "You have joined "+str(self.team),
                                "mails/team_approved.html",
                                {'to':self.owner,'team':self.team,'x': self},
                              list([self.owner]))
                return 'alert("done");'

            @wideio_action(icon="icon-remove",
                           possible=lambda s,
                           r: (r.user.is_authenticated() and r.user in s.team.members.all()))
            def deny(self, request):
                self.delete()
                from wioframework.utils import wio_send_email
                return 'alert("done");'
