#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *

@wideio_owned()
@wideiomodel
class TeamInvitation(models.Model):
    to_user = models.ForeignKey("accounts.UserAccount")
    team = models.ForeignKey("accounts.Team")

    def on_add(self, request):
        from wioframework.utils import wio_send_email
        wio_send_email(request, "email_team_invitation",
                            "You are invited to join the team "+str(self.team),
                            "mails/team_invitation.html",
                            {'from':self.owner,'to':self.to_user,'team':self.team,'x': self},
                          [self.to_user])

    class WIDEIO_Meta:
        CAN_TRANSFER=False
        DISABLE_VIEW_ACCOUNTING=True
        icon = "icon-group"
        permissions = dec.perm_for_logged_users_only
        MOCKS={
           'default':[
               {
                 'to_user':'user-0001',
                 'team': 'team-0000'
               }
           ]
        }

@wideio_relaction('accounts.models.Team',
                  icon="ion-paper-airplane",
                  possible=lambda s,
                  r: (r.user.is_authenticated() and r.user in s.admins.all()),
                  mimetype="text/html")
def invite_member(self, request):
    return HttpResponseRedirect(TeamInvitation.get_add_url() +"?team=%s" %(self.id))
