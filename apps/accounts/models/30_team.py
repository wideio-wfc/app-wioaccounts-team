#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *


# @wideio_followable("RelFollowTeam")
@wideio_publishable()
@wideio_owned()
@wideiomodel
class Team(models.Model):
    """
    A group of people that represents a private group.

    This group shall be closed groups.

    Team generally do not hold responsiblity directly.
    """
    API = True
    name = models.CharField(
        max_length=64,
        unique=True,
        blank=False,
        help_text="The name of your team")

    admins = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="team_admin_of",
        blank=True)
    members = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="member_of",
        blank=True)

    image = models.ForeignKey(
        'references.Image',
        null=False,
        help_text="An image used to identify your team visually", related_name="teams_with_logo")

    website = models.URLField(
        null=True,
        blank=True,
        help_text="A website if you have one")
    short_description = models.TextField()
    banner_image = models.ForeignKey('references.Image', null=True, help_text="A banner image to display on your page",
                                     related_name="teams_with_banner")

    pseudo_user = models.ForeignKey('accounts.UserAccount', null=True, blank=True)

    questions = models.ManyToManyField('network.QNA')

    # THE MOST IMPORTANT SCIENTIFIC QUESTIONS ACCORDING TO THE TEAM
    scientific_question1 = with_advanced_help_text(
        "By specifying scientific question you help others to identify what are your topics of interest. It shall be also noticed that you will see funding opportunities in priority when they relate to the questions that you have selected")(
        models.ForeignKey('science.Question', null=True, related_name='dedicated_team_rank_1', db_index=True))
    scientific_question2 = with_advanced_help_text(
        "By specifying scientific question you help others to identify what are your topics of interest. It shall be also noticed that you will see funding opportunities in priority when they relate to the questions that you have selected")(
        models.ForeignKey('science.Question', null=True, related_name='dedicated_team_rank_2', db_index=True))
    scientific_question3 = with_advanced_help_text(
        "By specifying scientific question you help others to identify what are your topics of interest. It shall be also noticed that you will see funding opportunities in priority when they relate to the questions that you have selected")(
        models.ForeignKey('science.Question', null=True, related_name='dedicated_team_rank_3', db_index=True))
    scientific_question4 = with_advanced_help_text(
        "By specifying scientific question you help others to identify what are your topics of interest. It shall be also noticed that you will see funding opportunities in priority when they relate to the questions that you have selected")(
        models.ForeignKey('science.Question', null=True, related_name='dedicated_team_rank_4', db_index=True))

    def on_add(self, request):
        self.members.add(request.user)
        self.admins.add(request.user)
        from accounts.models import UserAccount
        if self.pseudo_user == None:
            u = UserAccount()
            u.first_name = ""
            u.last_name = ""
            u.email_address = "team" + base64.b64encode(
                base64.b16decode(str(uuid.uuid1()).replace('-', '').upper())) + "@wide.io"
            u.username = u.email_address[:-8]
            u.is_active = False
            u.wiostate = 'A'
            u.metadata = {'team': self.id}
            u.save()
            self.pseudo_user = u
        ep = request.user.get_extended_profile()
        if ep.team is None:
            ep.team = self
            ep.save()
        self.save()

    def __unicode__(self):
        return unicode(self.name)

    def get_all_references(self):
        return []

    def get_document_types(self):
        return settings.INTEGRATION_DOCUMENT_TYPES

    @staticmethod
    def can_list(request):
        return True

    def can_view(self, request):
        return True

    def can_update(self, request):
        return request.user in self.admins.all()

    class WIDEIO_Meta:
        ADD_ENABLED = True
        icon = 'ion-person-stalker'
        form_exclude = ['admins', 'members', 'pseudo_user', 'questions']
        # permissions = dec.perm_read_logged_users_write_for_staff_only
        permissions = dec.perm_for_logged_users_only
        sort_enabled = ['name']
        search_enabled = "name"

        class Actions:
            @wideio_action(icon="ion-email",
                           mimetype="text/html",
                           xattrs="data-modal-link title=\"Send message\" ",
                           possible=lambda s,
                                           r: (r.user.is_authenticated() and r.user in s.members.all()))
            def send_message(self, request):
                from network.models import DirectMessage
                return HttpResponseRedirect(
                    DirectMessage.get_add_url() +
                    "?_AJAX=1&to=" +
                    self.id)

            @wideio_action("join",
                           mimetype="text/html",
                           icon="ion-log-in",
                           possible=lambda s, r: r.user.is_authenticated() and (r.user not in s.members.all()) and (
                                   s not in map(lambda t: t.team, r.user.teammembershiprequest_rev_owner.all()))
                           )
            def join_team(self, request):
                # from accounts.models import TeamMembershipRequest
                mr = TeamMembershipRequest()
                mr.owner = request.user
                mr.team = self
                mr.save()
                return {'_redirect': '/?redirect'}

            @wideio_action("set as main team",
                           icon="ion-log-in",
                           possible=lambda s,
                                           r: (r.user.is_authenticated() and r.user in s.members.all()))
            def set_as_main_team(self, request):
                ep = request.user.get_extended_profile()
                ep.team = self
                ep.save()

            @wideio_action("leave",
                           icon="ion-log-out",
                           possible=lambda s,
                                           r: (
                                   r.user.is_authenticated() and (r.user in s.members.all()) and (
                                   s.members.all().count() > 1)))
            def leave_team(self, request):
                # from accounts.models import TeamMembershipRequest
                self.members.delete(request.user)

        MOCKS = {
            'default': [
                {
                    '@id': 'team0000',
                    'name': 'sciteam0000',
                    'admins': [],
                    'members': [],
                    'image': None,
                    'website': 'http://www.univ.ac.uk/lab/team',
                    'short_description': 'a demo scientific team',
                    'scientific_question1': 'Question-0000'
                }
            ]
        }
